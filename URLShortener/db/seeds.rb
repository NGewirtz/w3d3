# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
a = User.create(email: '1@happy.com')
b = User.create(email: '2@happy.com')
c = User.create(email: '3f@happy.com')
d = User.create(email: '4@happy.com')


ShortenedUrl.destroy_all
e = ShortenedUrl.make_short_url(a, "1adfghdsfghfdsghfdghfdghjfh")
f = ShortenedUrl.make_short_url(a, "2adfgdgfdsfhfdsghfdghfdghjfh")
g = ShortenedUrl.make_short_url(b, "3dsfgdsfghfdsghfdghfdghjfh")
h = ShortenedUrl.make_short_url(b, "4adfghdsfghfdsghffdgdfgddghjfh")
i = ShortenedUrl.make_short_url(c, "5adfghdsfghfdsghffdgdfgddghjfh")
j = ShortenedUrl.make_short_url(d, "6adfghdsfghfdsghffdgdfgddghjfh")


Visit.destroy_all

Visit.record_visit!(a, i)
Visit.record_visit!(a, i)
Visit.record_visit!(b, e)
Visit.record_visit!(b, j)
Visit.record_visit!(c, i)
Visit.record_visit!(c, h)
Visit.record_visit!(c, f)
Visit.record_visit!(d, i)
Visit.record_visit!(d, j)
