class CreateShortenedUrl < ActiveRecord::Migration[5.1]
  def change
    create_table :shortened_urls do |t|
      t.string :long_url
      t.string :shortened_url
      t.integer :user_id
      t.timestamps
    end
    add_index(:shortened_urls, :shortened_url)
  end
end
