class ShortenedUrl < ApplicationRecord
  validates :shortened_url, :long_url, uniqueness: true, presence: true
  validates :user_id, presence: true

  belongs_to :user,
    class_name: 'User',
    foreign_key: :user_id,
    primary_key: :id

  has_many :visits,
    class_name: 'Visit',
    foreign_key: :shortened_url_id,
    primary_key: :id

  has_many :visitors,
    through: :visits,
    source: :users

  def self.random_code
    SecureRandom.urlsafe_base64
  end

  def self.make_short_url(user, long)
    short = ShortenedUrl.random_code
    ShortenedUrl.create(user_id: user.id, long_url: long, shortened_url: short)
  end

  def num_clicks
    Visit.where('shortened_url_id = ?', self.id).count
  end

  def num_uniques
    Visit.select(:user_id).where('shortened_url_id = ?', self.id).distinct.count
  end

  def num_recent_uniques
    Visit.select(:user_id).where('shortened_url_id = ? AND updated_at >= ?', self.id, 10.minutes.ago).distinct.count
  end
end
